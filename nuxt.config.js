const tls = require('tls')
tls.DEFAULT_ECDH_CURVE = 'auto'
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'lizart',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'layer&apos;s frontend with ssr under vuejs&apos;s nuxt' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', type: 'image/png', sizes:"57x57", href: '/apple-icon-57x57.png' },
      { rel: 'icon', type: 'image/png', sizes:"32x32", href: "/favicon-32x32.png" },
      // { rel: 'stylesheet', href: '/css/fonts.css' },
      // { rel: 'stylesheet', href: '/css/common.css'},
    ],
  },
  /*
  ** Customize the progress bar color
  */
  // loading: '~/components/loading.vue',
  /*
  ** Build configuration
  */
  build: {
    vendor: ['moment', 'axios'],
    extend (config, { isDev, isClient }) {

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
        const vueLoader = config.module.rules.find(function (module) {
               return module.test.toString() === '/\\.vue$/'
             })

         // Remove SVG from default rules
         config.module.rules.forEach((rule) => {
           if (rule.test.toString() === '/\\.(png|jpe?g|gif|svg)$/') {
             rule.test = /\.(png|jpe?g|gif)$/
           }
         })

         // Add svg inline loader configuration
         config.module.rules.push({
           test: /\.svg$/,
           loader: 'svg-inline-loader'
         })

         // Important to apply transforms on svg-inline:src
         vueLoader.options.transformToRequire['svg-inline'] = 'src'


    }
  },
  plugins: [
    {src: '~/plugins/svg-inline.js', ssr: true},
    // '~/plugins/bootstrap',
    // '~/plugins/vmask',
    // { src: '~/plugins/moment', ssr: false},
    // { src: '~/plugins/yametrik.js', ssr: false },
    // { src: '~/plugins/fbshare.js', ssr: false },
    // { src: '~/plugins/vkshare.js', ssr: false },
  ],

  css: [
    // 'bootstrap/dist/css/bootstrap.css',
    // 'bootstrap-vue/dist/bootstrap-vue.css',
    // '~/assets/css/fonts.css',
    // '~/assets/css/animation.css',
    // '~/assets/css/common.css'
  ]
}
